app.controller('LessonsCtrl', function ($scope, $stateParams , Lessons) {
    
    Lessons.all().then(function(data){
      // console.log(data)
      $scope.items = data;
    });

});


app.controller('LessonCtrl', function ($scope, $stateParams , Lessons,Poems , Words, $ionicModal, $timeout , $auth, ENDPOINT, $sce) {
    $auth.validateUser();
      
    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */
    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };

    Lessons.get($stateParams.id).then(function(data){

      $scope.lesson = data;
      
      // if( $scope.lesson.poem && $scope.lesson.poem.audio ){
      //   //$scope.audio= $sce.trustAsResourceUrl();
      // }
      if($scope.lesson.poet){
        $scope.poetContent=$sce.trustAsHtml($scope.lesson.poet.biography);

      }
      
      if(data.lines && data.lines.length > 0){
        $scope.lines = data.lines
      }else{
        if($scope.lesson.poem){
            $scope.lines = $scope.lesson.poem.stanzas[0].lines
        }
      }


    });

    var comments = []
    $scope.comments = comments

    $scope.comment = {
      text: ""
    }

    $scope.addComment = function(comment){
      Lessons.addComment($scope.lesson, comment ).then(function(data){
        //console.log(data)
        comments.push(data);

        $scope.comments = comments
        $scope.comment.text = "";
      })
    }

    $ionicModal.fromTemplateUrl('templates/lessons/comments.html', {
        id:'2',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.commentModal = modal;
    });


    $ionicModal.fromTemplateUrl('templates/lessons/detail.html', {
        id:'2',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.detailModal = modal;
    });


    $scope.openCommentModal = function(lesson_id){

      var l_id = lesson_id;

      Lessons.getComments(l_id).then(function(data){
        //console.log(data)
        if(!data.errors){
            $scope.comments = data
        }
      });
      $scope.commentModal.show();

      $scope.doRefresh = function(){
        Lessons.getComments(l_id).then(function(data){
            if(!data.errors){
                $scope.comments = data
            }
        })
        .finally(function() {
           // Stop the ion-refresher from spinning
           $scope.$broadcast('scroll.refreshComplete');
        });
      }

    }

    $scope.openModalDetail = function(){
        $scope.detailModal.show();
    }


});

app.controller('PoemsCtrl', function ($scope, $stateParams , Poems) {

    Poems.all().then(function(data){
      //console.log(data)      
      $scope.items = data;
    });
 
 
});

app.controller('PoemCtrl', function ($scope, $stateParams , Poems , Words, $ionicModal, $timeout, $sce, ENDPOINT) {
    

    Poems.get($stateParams.id).then(function(data){     

      $scope.poem = data;
      if( $scope.poem && $scope.poem.audio ){
        $scope.audio= $sce.trustAsResourceUrl(ENDPOINT.asset_api+$scope.poem.audio.audio.url);
      }
    });

    // Cleanup the modal when we're done with it
    // $scope.$on('$destroy', function() {
    //     $scope.wordModal.remove();
    // });

});


app.controller('GlossaryCtrl', function ($scope, $stateParams , Words , $ionicModal, $timeout) {
    //ionic.material.ink.displayEffect();
    //ionicMaterialInk.displayEffect();

    Words.all().then(function(data){
      $scope.glossary = data;
    });
    $scope.filter = function(term){
      return Words.filter(term)
    }


    $scope.scrollTop = function() {
      $ionicScrollDelegate.scrollTop();
    };




    $ionicModal.fromTemplateUrl('templates/poems/word_detail.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.wordModal = modal;
        $scope.activeTab = "0";
        $scope.isActive = function(tabId){
          return tabId == $scope.activeTab;
        }

        $scope.changeTab = function(tabId){
          $scope.activeTab = tabId;
        }
    });


    $scope.showWordDetail = function(term){
      
      Words.find_all_references(term.word_indexes[0], term.line_id).then(function(data){
        //console.log(data)
        $scope.words = data.word_references;
        $scope.words_title = data.word_reference_titles;
        $scope.wordModal.show();
      })



    }
});

﻿app.controller('AppCtrl', function ($scope,$rootScope, $ionicModal, $ionicPopover,
    $timeout, $auth, $state, $ionicPopup, $ionicHistory,
    $ionicLoading, ENDPOINT , Words) {
    // Form data for the login modal
    $scope.loginData = {};

    $rootScope.ENDPOINT = ENDPOINT;
    var initialize = function(){
        var navIcons = document.getElementsByClassName('ion-navicon');
        for (var i = 0; i < navIcons.length; i++) {
            navIcons.addEventListener('click', function () {
                this.classList.toggle('active');
            });
        }
    }

    initialize();
    // var fab = document.getElementById('fab');
    // fab.addEventListener('click', function () {
    //     //location.href = 'https://twitter.com/satish_vr2011';
    //     window.open('https://twitter.com/satish_vr2011', '_blank');
    // });

    $scope.goRegister= function(){
      
        $scope.modal.hide();
        $state.go('app.account')
    }


    $scope.openGlossary= function(signedIn){
        if(signedIn){
            $state.go('app.glossary');
        }else{

        var alertPopup = $ionicPopup.show({
            title: 'Section Protected',
            template: 'Only Limited to Approved User'
        });
               //initialize()
        $timeout(function() {
            $scope.modal.hide();
            alertPopup.close();

        }, 1000);

        }
    }

    $scope.openPoets= function(signedIn){
        if(signedIn){
            $state.go('app.poets');
        }else{

        var alertPopup = $ionicPopup.show({
            title: 'Section Protected',
            template: '   Only Limited to Approved User'
        });
               //initialize()
        $timeout(function() {
            $scope.modal.hide();
            alertPopup.close();

        }, 1000);

        }
    }


    $scope.openPoet= function(signedIn){
        if(signedIn){
            $state.go('app.poet');
        }else{

        var alertPopup = $ionicPopup.show({
            title: 'Section Protected',
            template: 'Only Limited to Approved User'
        });
               //initialize()
        $timeout(function() {
            $scope.modal.hide();
            alertPopup.close();

        }, 1000);

        }
    }


    $scope.openPoem= function(signedIn){
        if(signedIn){
            $state.go('app.poem');
        }else{

        var alertPopup = $ionicPopup.show({
            title: 'Section Protected',
            template: 'Only Limited to Approved User'
        });
               //initialize()
        $timeout(function() {
            $scope.modal.hide();
            alertPopup.close();

        }, 1000);

        }
    }

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
        document.getElementsByTagName("body")[0].classList.remove("modal-open")
    };

    // Open the login modal
    $scope.login = function() {
    $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
     $ionicLoading.show();
     $auth.submitLogin($scope.loginData)
      .then(function(user) {
        console.log(user);

        $ionicLoading.hide();
        $scope.closeLogin();

        var alertPopup = $ionicPopup.show({
            title: 'Success Login',
            template: 'Welcome ' + user.email
        });
        //initialize()
        $timeout(function() {
            $scope.modal.hide();
            alertPopup.close();
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableBack: true,
                historyRoot: true
            });
            $state.go('app.home');
        }, 1000);



      })
      .catch(function(resp) {
        $ionicLoading.hide();
        $scope.closeLogin();
        $scope.modal.hide();
        var html = "";
        angular.forEach(resp.errors , function(value , key){
            html += value + "<br >";
        })
        var alertPopup = $ionicPopup.alert({
            title: 'Login Error',
            template: html
        });
        
      });
    };

    $scope.logout = function() {
      $auth.signOut()
        .then(function(resp) {

            var alertPopup = $ionicPopup.alert({
                title: 'Log Out',
                template: 'Successfully Logged Out'
            });
            //initialize()
            $timeout(function() {
                ionicMaterialInk.displayEffect();
            }, 0);



          // handle success response
        })
        .catch(function(resp) {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: 'Logout Error'
            });
            //initialize()
            $timeout(function() {
                ionicMaterialInk.displayEffect();
            }, 0);

          // handle error response
        });
    };



     $scope.$on('auth:password-change-success', function(ev) {

       var alertPopup = $ionicPopup.show({
           title: 'Update Success',
           template: "Your password has been successfully updated!"
       });
       $timeout(function() {
           alertPopup.close();
           $ionicHistory.clearCache();
           $ionicHistory.clearHistory();
           $state.go('app.home');
       }, 2000);
     });
     $scope.$on('auth:password-change-error', function(ev, reason) {

       var alertPopup = $ionicPopup.show({
           title: 'Update Error',
           template: "Update failed: " + reason.errors[0]
       });
    });

    $rootScope.$on('loading:show', function() {
        //console.log("bababa")
        $ionicLoading.show({template: 'Loading..'});
    })
    $rootScope.$on('error:no_internet', function() {
        //console.log("bababa")
        $ionicLoading.hide();
        $ionicLoading.show({template: 'No Internet Connection'});
    })
    $rootScope.$on('error:response_error', function() {
        //console.log("bababa")
        $ionicLoading.hide();
        //$ionicLoading.show({template: 'Something wrong happen to get the data'});
        $timeout(function() {
            $ionicLoading.hide();
        }, 2000);
    })

    $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide();
    })



    // $scope.$on('auth:registration-email-success', function(ev, user) {
    //
    //
    //     $ionicLoading.hide();
    //     $ionicHistory.clearCache();
    //     $ionicHistory.clearHistory();
    //     $ionicHistory.nextViewOptions({
    //         disableBack: true,
    //         historyRoot: true
    //     });
    //     $state.go('app.home');
    //     //document.getElementById('loggedInUser').innerHTML = user.name;
    //
    //     // var alertPopup = $ionicPopup.alert({
    //     //     title: 'Success Login',
    //     //     template: 'Welcome ' + user.email
    //     // });
    //     // //initialize()
    //     // $timeout(function() {
    //     //     //ionic.material.ink.displayEffect();
    //     //     ionicMaterialInk.displayEffect();
    //     // }, 0);
    //
    // });
    //
    $scope.$on('auth:registration-email-error', function(ev, user) {


        // $ionicLoading.hide();
        // $ionicHistory.clearCache();
        // $ionicHistory.clearHistory();
        // $ionicHistory.nextViewOptions({
        //     disableBack: true,
        //     historyRoot: true
        // });
        // $state.go('app.home');
        //document.getElementById('loggedInUser').innerHTML = user.name;

        var alertPopup = $ionicPopup.alert({
            title: 'Success Login',
            template: 'Welcome ' + user.email
        });
        
    });


    $scope.$on('auth:registration-email-success', function(ev, user) {


        // $ionicLoading.hide();
        // $ionicHistory.clearCache();
        // $ionicHistory.clearHistory();
        // $ionicHistory.nextViewOptions({
        //     disableBack: true,
        //     historyRoot: true
        // });
        // $state.go('app.home');
        //document.getElementById('loggedInUser').innerHTML = user.name;

        var alertPopup = $ionicPopup.alert({
            title: 'Registraion Successful ',
            template: 'Welcome ' + user.email
        });        

    });

    $scope.$on('auth:session-expired', function(ev) {

        var alertPopup = $ionicPopup.alert({
            title: 'Notice',
            template: 'Session Expired'
        });        
    });



    $ionicModal.fromTemplateUrl('templates/poems/word_detail.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.wordModal = modal;
        $scope.activeTab = 0;
        $scope.isActive = function(tabId){
          return tabId == $scope.activeTab;
        }

        $scope.changeTab = function(tabId){
          $scope.activeTab = tabId;
        }
    });


    $scope.openWordModal = function(word_index, line_id,id) {
        
        Words.find_all_references(word_index, line_id).then(function(data){
          $scope.words = data.word_references;
          $scope.words_title = data.word_reference_titles;
        })
        $scope.wordModal.show();


    };


    // $ionicModal.fromTemplateUrl('templates/profile.html', {
    //     scope: $scope,
    //     animation: 'slide-in-up'
    // }).then(function(modal) {
    //     $scope.profileModal = modal;

    // });

    // $scope.openProfileModal= function(){
    //   $scope.profileModal.show();

    // }

});

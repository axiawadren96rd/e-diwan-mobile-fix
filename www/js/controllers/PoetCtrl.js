app.controller('PoetsCtrl', function ($scope, $stateParams , Poets) {
    
    Poets.all().then(function(data){
      //console.log(data)
      $scope.items = data;
    });


});


app.controller('PoetCtrl', function ($scope, $stateParams , Poets,$sce) {
    
    Poets.get($stateParams.id).then(function(data){
      // console.log(data)
      $scope.poet = data;
      $scope.myHTML=$sce.trustAsHtml($scope.poet.biography);
    });


    });

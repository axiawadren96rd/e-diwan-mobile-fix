// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('e-diwan', ['ionic',
    'e-diwan.constants',
    'ng-token-auth' ]);

app.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.config(function($sceDelegateProvider,ENDPOINT) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    ENDPOINT.asset_api + '/**'
  ]);

  // // The blacklist overrides the whitelist so the open redirect here is blocked.
  // $sceDelegateProvider.resourceUrlBlacklist([
  //   'http://myapp.example.com/clickThru**'
  // ]);
})


app.config(function($authProvider, ENDPOINT) {
  $authProvider.configure({
    apiUrl: ENDPOINT.auth_api ,
    storage: "localStorage", 
    forceValidateToken:      false,

    cookieOps: {
        path: "/",
        expires: 9999,
        expirationUnit: 'days',
        secure: false,
        domain: ENDPOINT.domain_name
      },
  });
})

app.config(function($httpProvider) {
  $httpProvider.interceptors.push(function($rootScope) {
    return {
      request: function(config) {
        //console.log(config.url)
        //console.log("tapped" + config.url)
        if(!config.url.startsWith("templates/")){
            $rootScope.$broadcast('loading:show')
        }
        return config;
      },

      responseError: function (rejection) {
        if (rejection.status == 0) {
            // put whatever behavior you would like to happen
        }
        if (rejection.status == -1) {
            $rootScope.$broadcast('error:no_internet')
        }else{
            $rootScope.$broadcast('error:response_error')
        }
        // .......
        return rejection;
      },


      response: function(response) {

        if(!response.config.url.startsWith("templates/")){
            //console.log(response.config.url)
            $rootScope.$broadcast('loading:hide')
        }
        return response;

      }
    }
  })
})

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })
    .state('app.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            }
        }
    })
    .state('app.poems', {
        url: '/poems',
        views: {
            'menuContent': {
                templateUrl: 'templates/poems/index.html',
                controller: 'PoemsCtrl'
            }
        }
    })

    .state('app.poem', {
        url: '/poems/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/poems/show.html',
                controller: 'PoemCtrl'
            }
        }
    })

    .state('app.poets', {
        url: '/poets',
        views: {
            'menuContent': {
                templateUrl: 'templates/poets/index.html',
                controller: 'PoetsCtrl'
            }
        }
    })
    .state('app.poet', {
        url: '/poets/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/poets/show.html',
                controller: 'PoetCtrl'
            }
        }
    })


    .state('app.lessons', {
        url: '/lessons',
        views: {
            'menuContent': {
                templateUrl: 'templates/lessons/index.html',
                controller: 'LessonsCtrl'
            }
        }
    })
    .state('app.lesson', {
        url: '/lessons/:id',
        views: {
            'menuContent': {
                templateUrl: 'templates/lessons/show.html',
                controller: 'LessonCtrl'
            }
        }
    })

    .state('app.glossary', {
        url: '/glossary',
        views: {
            'menuContent': {
                templateUrl: 'templates/glossary/index.html',
                controller: 'GlossaryCtrl'
            }
        }
    })
    .state('app.settings', {
        url: '/settings',
        views: {
            'menuContent': {
                templateUrl: 'templates/settings/index.html',
                controller: 'SettingsCtrl'
              
            }
        }
    })


    .state('app.account', {
        url: '/account',
        views: {
            'menuContent': {
                templateUrl: 'templates/settings/account.html',
                controller: 'AccountCtrl'
              
            }
        }
    })


    .state('app.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'templates/settings/profile.html',
                controller: 'ProfileCtrl'
              
            }
        }
    })


    .state('app.about', {
        url: '/about',
        views: {
            'menuContent': {
                templateUrl: 'templates/settings/about.html',
                controller: 'AboutCtrl'
              
            }
        }
    })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/home');
});

app.factory('Lessons', function(ENDPOINT, $http) {

  return {
    all: function() {
      return $http.get(ENDPOINT.api + '/lessons.json').then(function (response) {            
        return response.data;
      });
    },

    preview: function(){
      return $http.get(ENDPOINT.api + '/free_lessons.json').then(function (response) {            
        return response.data;
      });
    },
    
    get: function(id){
      return $http.get(ENDPOINT.api + '/lessons/'+id+'.json').then(function (response) {            
        return response.data;
      });
    },
    getComments: function(lesson_id){
      return $http.get(ENDPOINT.api + '/lessons/' + lesson_id + '/comments.json').then(function(response){
        //console.log(response)
        return response.data;
      });

    },

    addComment: function(lesson , comment){

      return $http.post(ENDPOINT.api + '/lessons/' + lesson.id + '/comments.json',
        { 
          comment: {
            comment: comment.text, 
            lesson_id : lesson.id 
          }
        }).then(function(response){
        //console.log(response)
        return response.data;
      });
    }

  };
})



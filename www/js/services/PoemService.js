app.factory('Poems', function(ENDPOINT, $http) {

  return {
    all: function() {      
      return $http.get(ENDPOINT.api + '/poems.json').then(function (response) {            
        return response.data;
      });
    },
    
    get: function(id){
      return $http.get(ENDPOINT.api + '/poems/'+id+'.json').then(function (response) {            
        return response.data;
      });
    }

    // // get returns a promise of like
    // get: function (id) {
    //     // then allows transforming a promise of array of likes into a promise of something else
    //     return poems.then(function(data) {
    //         // transform the array of likes into a single like
    //         for (var i = 0; i < data.length; i++) {
    //             if (data[i].id === parseInt(id)) {
    //                 return data[i];
    //             }
    //         }
    //         return null;
    //     })
    // }
  };
})



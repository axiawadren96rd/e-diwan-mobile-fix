app.factory('Poets', function(ENDPOINT, $http) {

  return {
    
      all: function() {
        return $http.get(ENDPOINT.api + '/poets.json').then(function (response) {            
          return response.data;
        });
      },
      
      get: function(id){
        return $http.get(ENDPOINT.api + '/poets/'+id+'.json').then(function (response) {            
          return response.data;
        });
      }

};
})



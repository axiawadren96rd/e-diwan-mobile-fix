app.factory('Words', function(ENDPOINT,$http) {
  return {
    all: function(){
      return $http.get(ENDPOINT.api + '/word_references/search.json').then(function (response) {                    
        return response.data;
      });
    },
    filter: function(term) {
      return $http.get(ENDPOINT.api + '/word_references/search.json?query=' + term).then(function (response) {            
        //console.log(response.data)
        return response.data;
      });
    },
    find_all_references: function(word_index, line_id) {
      return $http.get(ENDPOINT.api + '/word_references/find_all_references.json?word_index=' + word_index + "&line_id=" + line_id).then(function (response) {            
        //console.log(response.data)
        return response.data;
      });
    }
    
      
  };
})



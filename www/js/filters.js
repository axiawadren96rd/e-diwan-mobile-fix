app.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);

app.filter('replaceServerImageSrc', function (ENDPOINT) {
  return function (input) {    
    if(input){
      var output = input.replace(new RegExp("src=\"/uploads/ckeditor/", 'g'), "src=\"" + ENDPOINT.asset_api + "/uploads/ckeditor/")          
      return output;
    }
  };
});



app.directive('ionSearch', function( $ionicScrollDelegate) {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      getData: '&source',
      model: '=?',
      search: '=?filter'
    },
    link: function(scope, element, attrs) {
      attrs.minLength = attrs.minLength-1 || 0;
      
      scope.placeholder = "Search";
      
      scope.search = {value: null};
      if (attrs.class)
        element.addClass(attrs.class);

      if (attrs.source) {
        scope.$watch('search.value', function (newValue, oldValue) {
          if (newValue != null && newValue.length > attrs.minLength) {
            scope.getData({str: newValue}).then(function (results) {
              scope.model = results;
              $ionicScrollDelegate.scrollTop();
            });
          } else {
            scope.model = scope.getData({str: null});
            // scope.model = [];
          }
        });
      }

      scope.clearSearch = function() {
        scope.search.value = null;
      };
    },
    template: '<div class="bar bar-subheader bar-clear list">' +
              '  <div class="item item-input-inset">' +
              '    <i class="icon ion-ios-search placeholder-icon"></i>' +
              '    <label class="light-bg item-input-wrapper" focus-me>' +
              '      <input type="search" placeholder="{{placeholder}}" ng-model="search.value">' +
              '    </label>' +
              '    <i ng-click="clearSearch()" class="icon ion-close placeholder-icon" ng-if="search.value.length"></i>' +
              '  </div>' +
              '</div>'
  };
})
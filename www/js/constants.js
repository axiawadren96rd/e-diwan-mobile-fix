angular.module('e-diwan.constants', [])
.constant('ENDPOINT', { 
  
  // auth_api: "http://localhost:3000/api/v1" , 
  // api: "http://localhost:3000/api/v1" , 
  // asset_api: "http://localhost:3000/" , 
   auth_api: "http://e-diwan.namlite.com/api/v1" , 
   api: "http://e-diwan.namlite.com/api/v1" , 
   asset_api: "http://e-diwan.namlite.com" , 
  dev: "http://localhost:3000/api/v1" , 
  prod: "http://e-diwan.namlite.com:3000/api/v1" ,
  domain_name: "e-diwan.namlite.com" })




.constant('TRANSLATION', {
  en: {
    TERMS: "Terms",
    TERM: "Term",
    SEARCH_PLACEHOLDER: "Search"
  }
})
ionic plugin add cordova-plugin-whitelist --save
ionic plugin add ionic-plugin-keyboard --save

Prepareing APK:
---------------

$ cordova build --release android

$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk com.namlite.e-diwan

password: $-D93an

$ zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk e-diwan-0.0.6.apk


Publish: 
--------

https://play.google.com/apps/publish/


